
# Noisy 

Noisy is a command-line tool for generating random DNS and HTTP/S internet traffic noise. 

A ["correlation attack"](https://github.com/Attacks-on-Tor/Attacks-on-Tor#correlation-attacks) is a way that powerful adversaries can deanonymize Tor users. The traffic that goes "in" and "out" of the Tor network can be correlated to break Tor's anonymity, and this risk is all the more realistic with [advances in Machine Learning](https://arxiv.org/pdf/1808.07285v1.pdf). 

The Tor Project [officially recommends](https://blog.torproject.org/new-low-cost-traffic-analysis-attacks-mitigations/) to "do multiple things at once with your Tor client" to counter correlation attacks: "an adversary that externally observes Tor client traffic to a Tor Guard node will have a significantly harder time performing classification if that Tor client is doing multiple things at the same time." An [analysis](https://medium.com/beyond-install-tor-signal/case-file-jeremy-hammond-514facc780b8) of how a correlation attack was used in a trial notes "create random internet traffic when using Tor — ideally by running a script." A "script" is a way of automating computer activity, and it causes far more randomization than manually doing "multiple things", such as by loading a video in an additional Tor Browser tab. 

This fork of [Noisy](https://github.com/1tayH/noisy) is intended to mitigate against correlation attacks which do not use the latest state-of-the-art techniques developed by researchers (for those we would need another software, which has not been written yet, to our knowledge). We expect Noisy to mitigate against correlation attacks by many powerful adversaries worldwide, but because of the simple techniques it uses - only HTTP/S requests, not taking into account existing Tor traffic to try to blend into it - it is definitely not foolproof.

You have probably heard to that it is a bad idea to download a script from the internet and then run it on your computer, if you can't first understand what it is doing. We agree. Here are some ways that you can establish trust with this code:
- The code is owned by the project anarsec.guide, and any code changes ("commits") would need to be digitally signed by them to display a green "Verified" mark in the [commit history](https://0xacab.org/anarsec/noisy/-/commits/master).
- If you don't want to place trust in AnarSec, have a friend who can read python look over the 274 lines of code to verify that nothing malicious is happening. 

# Usage

On either Whonix Workstation or Tails, start by downloading the code as a .zip file. 
![](download.png)

## Download Verification

First, we will ensure that the file is what it should be by making a ["checksum"](https://www.anarsec.guide/glossary/#checksums-fingerprints) and verifying it. 

On Tails, [use the program GtkHash](https://tails.net/doc/encryption_and_privacy/checksums/index.en.html) to calculate the SHA512 hash of the .zip file. The output should be exactly what is listed at the [anarsec.guide](https://www.anarsec.guide/glossary/#checksums-fingerprints) project. 

On Whonix Workstation, use `sha512sum` on the command line. 

Because of the way that checksums work, you can just verify the beginning and the end - you don't need to compare every single character. 

Once the .zip is verified, extract it. In the file manager, right-click on the `noisy-master.zip` file, and select "Extract here".  

## Installing "dependencies"

Noisy has one "dependency" that must be installed for it to work properly, Python non-standard module "requests".

Whonix Workstation and Tails both have this module installed by default (via the package `python3-requests`), so you shouldn't need to install any dependencies before using Noisy.

## Running the script 

A script must be run via the "terminal", which is also known as the "command line". In the file manager, enter the `noisy-master` folder. We will now open a terminal in that folder by right-clicking white space in the file manager, and selecting "Open Terminal Here".

**On Tails**, type (or copy paste) exactly the following command into the terminal:
`torsocks python3 noisy.py --config config.json`

**On Whonix Workstation**, type (or copy paste) exactly the following command into the terminal:
`python3 noisy.py --config config.json`

This will run the noisy script based on the default configuration file provided, over the Tor network. Output will list the websites that are being visited, and look something like:
```
INFO:root:Visiting https://mx.ebay.com
INFO:root:Visiting https://ve.ebay.com
INFO:root:Visiting https://do.ebay.com
```

Start this script right after you turn on your computer, and only stop the script from running when you will power off your computer, by closing the terminal window. Simply navigate to where you decide to save the `noisy-master` folder, and follow the above instructions for opening a terminal and entering the "run the script" command. 

If you are familiar with the command-line interface, the program can accept a number of arguments:
```
$ python3 noisy.py --help
usage: noisy.py [-h] [--log -l] --config -c [--timeout -t]

optional arguments:
  -h, --help    show this help message and exit
  --log -l      logging level
  --config -c   config file
  --timeout -t  for how long the crawler should be running, in seconds
```

In the configuration file, do not set a very brief amount of time between requests, because this will decrease randomness. The request itself takes a certain amount of time to complete, and if the value of `max_sleep` minus `min_sleep` is smaller than this amount of time, the frequency of requests will be throttled by how fast the requests take to complete rather than by the values in the configuration file. If you want more traffic, simply run the script again in a second terminal window (or third, fourth, etc.). 

## Guard node considerations

For the script to be useful, it needs to use the same Tor "Guard node" as your sensitive activity. Otherwise, someone looking at your Tor traffic can easily isolate the traffic they are interested in for correlation. For this reason, running this script from a Rasperry Pi 24/7 isn't particularly helpful, because this device won't be using the same Guard node as your Tails or Whonix Workstation device.  

# Changes

Noisy is forked from [Github](https://github.com/1tayH/noisy). A [single change](https://0xacab.org/anarsec/noisy/-/commit/b1c7e5758cb3ee49e66208a82409627c3ead899c) in the python script was made, so that wait times between requests can be configured as fractions of seconds instead of whole seconds, increasing randomness. 

The configuration file has the following change:
- The time between requests is a random amount from 0.5 to 3 seconds (instead of from 3 to 6 seconds). 

# Troubleshooting

## Error output about "import requests"

It sounds like your system doesn't have the Python non-standard module "requests" installed. We recommend using Noisy on either Tails or Whonix Workstation, where it is installed by default. 

## The SHA512 hash isn't correct

Verify that you downloaded the .zip file, and not .tar.gz, or another compression type. If it is still incorrect, delete the download, then download it again using a new Tor circuit. 
